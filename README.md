# Introduction

This repository contains material used in workshops on [Morpheus](https://morpheus.gitlab.io).

# Tutorials

## [Tutorial 1](./tutorial-intro/README.md)

Illustrated tutorial on the following topics:
- Overview of GUI
- My first model
- Cell-based modeling
- Multi-scale modeling

See ['tutorial-intro'](./tutorial-intro/README.md) folder. 

## [Tutorial 2](./tutorial-paramsweep/README.md)

Illustrated tutorial exploring:
- How to do parameter exploration with Morpheus
- How to analyse parameter sweep in python notebooks

See ['tutorial-paramsweep'](./tutorial-paramsweep/README.md) folder. 

## [Extra tutorial assignments](./extras)

See assignments are ['extras'](./extras) folder.

# Presentation

[Here](https://gitlab.com/wdeback/Workshop-Morpheus/blob/master/slides/2017_Nottingham_Morpheus.pdf) are the slides in [PDF format](https://gitlab.com/wdeback/Workshop-Morpheus/blob/master/slides/2017_Nottingham_Morpheus.pdf).

Otherwise, the slides for the presentation are in the ['slides'](https://gitlab.com/wdeback/Workshop-Morpheus/tree/master/slides) folder (in pdf and keynote format).


# Documentation

- [**Documentation**](https://imc.zih.tu-dresden.de/wiki/morpheus/doku.php?id=documentation:documentation): Docs, cheat sheets etc. (slightly outdated)
  
- [**User forum**](https://groups.google.com/forum/#!forum/morpheus-users): Modeling questions for users.

- [**GitLab issue tracker**](https://gitlab.com/morpheus.lab/morpheus/issues): Bug reports, feature requests, mainlyfor advanced users and developers. 


# Installation of Morpheus

To install on your own machine from source, see below:

- To install from binary packages, see the [download page](https://imc.zih.tu-dresden.de/wiki/morpheus/doku.php?id=download:download) at the [homepage]( https://imc.zih.tu-dresden.de/wiki/morpheus/doku.php)
 or use the direct links below:

| OS | Link |
| --- | --- |
| Windows | https://imc.zih.tu-dresden.de//morpheus/packages/windows/public/morpheus-1.9.3.Windows.b170911.exe |
| Mac OSX | https://cloudstore.zih.tu-dresden.de/index.php/s/7YWaptn7LKNKVLO/download |
| Ubuntu Trusty | https://imc.zih.tu-dresden.de/morpheus/packages/dists/trusty/public/binary-amd64/morpheus_1.9.2_amd64~b170503.deb |
| Ubuntu Vivid | https://imc.zih.tu-dresden.de/morpheus/packages/dists/vivid/public/binary-amd64/morpheus_1.9.2_amd64~b170503.deb |
| Ubuntu Wily | https://imc.zih.tu-dresden.de/morpheus/packages/dists/wily/public/binary-amd64/morpheus_1.9.2_amd64~b170503.deb |
| Ubuntu Xenial | https://imc.zih.tu-dresden.de/morpheus/packages/dists/xenial/public/binary-amd64/morpheus_1.9.2_amd64~b170503.deb |
| Debian Jessie | https://imc.zih.tu-dresden.de/morpheus/packages/dists/jessie/public/binary-amd64/morpheus_1.9.2_amd64~b170503.deb |


- To buid from source, see instructions at https://gitlab.com/morpheus.lab/morpheus.


# Download as zip

Download the whole repository as [a zip file](https://gitlab.com/wdeback/Workshop-Morpheus/repository/archive.zip?ref=master).


# Authors

## Morpheus

-  Jörn Starruss (1) and Walter de Back (1,2)

  (1) Center for Information Services and High Performance Computing, TU Dresden

  (2) Institute for Medical Informatics and Biometry, Faculty of Medicine, TU Dresden

## Presentation and tutorial:

-  Walter de Back  [@wdeback](http://twitter.com/wdeback), [homepage](http://walter.deback.net)

   Institute for Medical Informatics and Biometry, Faculty of Medicine, TU Dresden,

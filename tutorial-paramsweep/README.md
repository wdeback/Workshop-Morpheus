Morpheus: an illustrated tutorial
-----------------


## Introduction

In simulation studies, you often want to explore the effect that particular choices of parameter values have on the simulation result. 
In this tutorial, we will see how to systematically explore parameter values in Morpheus, using a feature which we call `ParamSweep`.

Specifically, we will see how to:

- [Select parameters to include in the sweep](#select-paramsters)
- [Specify parameter ranges](#specify-value-range)
- [Run a parameter sweep](#run-a-paramsweep)
- [Visualize sets of simulation results](#imagetable)
- [Use a python notebook for further analysis](#python-notebook)


## Open model

For purpose of illustration, we will use a simple [toggle switch model](./models/toggle_switch_paramsweep.xml) that you can find under
`tutorial-paramsweep/models/toggle_switch_paramsweep.xml`.

---

> Download this model: [toggle\_switch\_paramsweep.xml](./models/toggle_switch_paramsweep.xml) 

---

Open the model and select `Global` section where the ODE system with `Variables` `u` and `v` is defined. 

<img src="./images/1.png" width="80%" />


## Select parameters

We will systematically change the value of parameter `a1`  and observe the effect this has on the simulation result.

To select a parameter for a sweep:

- select the parameter in the **Editor** panel **or** select its value in the **Attribute** panel,
- select **ParamSweep** from the context menu (right-click)

Once selected, a *fast-forward* icon will be shown in front of the parameter.

Here, please select either the `Constant` `a1` (or equivalently, `a2`) for the `ParamSweep`:

<img src="./images/2.png" width="60%" />

Afterwards, it will appear in the `ParamSweep` section of the **Documents** panel.

## Specify value ranges

Next, we want to specify the range of values and their intervals for the parameter. For each (combination of) parameter values, a simulation will be executed.

If you select `ParamSweep` section from the **Documents** panel, the **ParamSweep** panel shows up in the center:

<img src="./images/3.png" width="80%" />

Here, under *Attribute*, you see the XML path of the selected parameter. Under *Values*, we can now specify the value range.

Above, I specified `1:#10:3` which reads: "*go from 1 to 3 in 10 steps*".

Morpheus supports a number of syntaxes to help specify long sequences. Using the following syntax, sequences will be automatically expanded: 

| Description | Syntax | Example | Expanded | 
| -------- | -------- | ----- | ----- | 
| Values | val;val;val  | `0;2;10` (semicolon)| `0;2;10`
| Interval spacing   | min:width:max  | `0:2:10` | `0;2;4;6;8;10`
| Number of steps | min:#steps:max  | `0:#2:10` | `0;5;10`
| Number of logarithmic steps | min:#stepslog:max  | `1:#2log:100` | `1;10;100`

## Multiple parameters

You can also select **multiple parameters** in your sweep. By default, these will be executed combinatorially. 

To change parameters simultaneously instead, drag one parameter over another to **pair** them. In this case, they should be of the same length (else, the shortest sequence will be used).

## Random seed

For stochastic simulations, you often want to re-run the same parameter set under varying random seeds. 
In this case, select `Time / RandomSeed` as an additional parameter in the `ParamSweep`.

# Run a `ParamSweep`

To execute a parameter sweep, select `Start` (F8) from within the `ParamSweep` panel.

A window will appear thaht gives an overview of the parameter sets that will be simulated. Please note the **number of jobs** that will be executed (as this can easily explode in combinatorial parameter sweeps):

<img src="./images/4.png" width="40%" />

If you are satisfied, select `Submit jobs` to start this batch of simulations.

## Sweep results

Results for a parameter sweeps are stored in a separate folder.

You can browse through these results from the **JobQueue** panel:

<img src="./images/6.png" width="60%" />

Un the root folder of the sweep, a file describing the sweep is stored, called `sweep_summary.txt`. This can be used for posthoc analysis (i.e. in the python notebook as we will see later): 

```
# Parametersweep Date: Tue Sep 12 2017 Time: 11:12:30
# Starting 11 Jobs
# Parameter P0 : MorpheusModel/Global/System/Constant[symbol=a1]/@value
# JOB LIST
# Folder	P0

Toggle-Switch_sweep_59/Toggle-Switch_6394	1
Toggle-Switch_sweep_59/Toggle-Switch_6395	1.2
Toggle-Switch_sweep_59/Toggle-Switch_6396	1.4
Toggle-Switch_sweep_59/Toggle-Switch_6397	1.6
Toggle-Switch_sweep_59/Toggle-Switch_6398	1.8
Toggle-Switch_sweep_59/Toggle-Switch_6399	2
Toggle-Switch_sweep_59/Toggle-Switch_6400	2.2
Toggle-Switch_sweep_59/Toggle-Switch_6401	2.4
Toggle-Switch_sweep_59/Toggle-Switch_6402	2.6
Toggle-Switch_sweep_59/Toggle-Switch_6403	2.8
Toggle-Switch_sweep_59/Toggle-Switch_6404	3
```


## Parallel computing

If you have a multi-core computer, you can run simulations in parallel.

To enable this, go to `Settings` (Linux, Windows) or `Preferences` (Mac). In the `Local` tab, set the desired number of **Concurrent jobs** (i.e. equal to the number of cores or threads): 

<img src="./images/5.png" width="60%" />

> For computationally heavy multi-ODE or PDE simulations, you can also switch on **Threads per job** to enable openMP multithreading. Single ODE models (such as the one here) or pure CPM simulations do not benefit from multithreading. 
Typically, however, for batch processing, [*embarrasingly parallel*](https://en.wikipedia.org/wiki/Embarrassingly_parallel) computing is faster.

# ImageTable

To have a quick overview of results of your parameter sweep, you may want to a table of output images. Morpheus includes a feature to generate a table of images (or videos) that can be viewed with a `html5`-enabled web browser. 

To generate such a table, select the root of the sweep folder in the **JobQueue**. Then, from the the **ParamSweep** view, click the **ImageTable** button:

<img src="./images/7.png" width="60%" />

Next, select an image in one of the sweep results, e.g. `logger_plot_time_u_025.0.png` and press **Generate**:

<img src="./images/8.png" width="40%" />

A html file `table_logger_plot_time_u_025.html` will be generated and opened in a webbrowser. The table displays the output image for each folder in the sweep, together with the corresponding parameter value:

<img src="./images/9.png" width="80%" />

Here, we see a switch on expression around the value `a1 = 2.0`, as expected.

# Python notebook

For more in-depth analysis of parameter sweeps, you have to resort to external tools such as R, Python, MatLab or even Excel. 

Since Jupyter or Ipython notebooks are increasingly popular for data science, I include an example of how to read in, select and visualize results from parameter sweep into a notebook. 

<img src="./images/10.png" width="80%" />


---

> Download the notebook (.ipynb): [ParamSweep-Analysis-ToggleSwitch.ipynb](./notebook/ParamSweep-Analysis-ToggleSwitch.ipynb) 

> or the html-version: [ParamSweep-Analysis-ToggleSwitch.html](./notebook/ParamSweep-Analysis-ToggleSwitch.html) 

---




<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="3">
    <Description>
        <Details>Model in which:
- cells have an internal model for the 'circadian' model (Property "c" changes as a simple cosine)
- the state in the circadian model modulates the uptake of a drug (Field "d"): uptake is proportional to "c*d"
- the diffusive drug "d" is attributed at a particular time (t=100) and decays
- cells die when certain a certain amount "max_value" of "d" is accumulated inside the cell.

There are two types of cells (cell and tumor) that are identical, except for the period of the circadian model ("tau1" and "tau2").

As a result, one celltype dies earlier from the drug as the other.

</Details>
        <Title>Nottingham model</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="100, 100, 0"/>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <Global>
        <Constant symbol="tau1" value="50"/>
        <Constant symbol="tau2" value="25"/>
        <Constant symbol="max_value" value="0.20"/>
        <Constant symbol="c" value="0"/>
        <Constant symbol="d_s" value="0"/>
        <Constant symbol="d_i" value="0"/>
        <Field symbol="d" value="0" name="drug">
            <Diffusion rate="0"/>
        </Field>
        <System solver="heun" time-step="1.0">
            <DiffEqn symbol-ref="d">
                <Expression>if(time == 50, 
1, 
-0.001*d - 0.001*c*d

)</Expression>
            </DiffEqn>
        </System>
    </Global>
    <CellTypes>
        <CellType class="biological" name="cells">
            <Property symbol="c" value="0" name="circadian"/>
            <Property symbol="d_i" value="0" name="instantaneous drug"/>
            <Property symbol="d_s" value="0" name="sum of drug over time"/>
            <System solver="euler" time-step="1.0">
                <Rule symbol-ref="c">
                    <Expression>(cos(time / tau1)+1) / 2</Expression>
                </Rule>
            </System>
            <VolumeConstraint target="100" strength="1"/>
            <SurfaceConstraint target="1" mode="aspherity" strength="1"/>
            <CellDeath>
                <Condition>d_s > max_value</Condition>
            </CellDeath>
            <Equation symbol-ref="d_s">
                <Expression>d_s + d_i*0.001*c</Expression>
            </Equation>
            <CellReporter>
                <Input value="d"/>
                <Output symbol-ref="d_i" mapping="average"/>
            </CellReporter>
        </CellType>
        <CellType class="biological" name="tumor">
            <Property symbol="c" value="0" name="circadian"/>
            <System solver="euler" time-step="1.0">
                <Rule symbol-ref="c">
                    <Expression>(cos(time / tau2)+1) / 2</Expression>
                </Rule>
            </System>
            <VolumeConstraint target="100" strength="1"/>
            <SurfaceConstraint target="1" mode="aspherity" strength="1"/>
            <CellDeath>
                <Condition>d_s > max_value</Condition>
            </CellDeath>
            <Property symbol="d_s" value="0" name="sum of drug over time"/>
            <Property symbol="d_i" value="0" name="instantaneous drug"/>
            <Equation symbol-ref="d_s">
                <Expression>d_s + d_i*0.001*c</Expression>
            </Equation>
            <CellReporter>
                <Input value="d"/>
                <Output symbol-ref="d_i" mapping="average"/>
            </CellReporter>
        </CellType>
    </CellTypes>
    <Analysis>
        <Logger time-step="1">
            <Input>
                <Symbol symbol-ref="c"/>
                <Symbol symbol-ref="d_s"/>
                <Symbol symbol-ref="d_i"/>
            </Input>
            <Output>
                <TextOutput file-separation="cell"/>
            </Output>
            <Plots>
                <Plot time-step="100">
                    <Style style="lines"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="c"/>
                    </Y-axis>
                </Plot>
                <Plot time-step="-1">
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="d_s"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="cell.type"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <Gnuplotter time-step="20">
            <Terminal opacity="1.0" name="png"/>
            <Plot>
                <Cells value="c">
                    <ColorMap>
                        <Color value="1.0" color="red"/>
                        <Color value="0.5" color="yellow"/>
                        <Color value="0.0" color="white"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot>
                <Field symbol-ref="d" min="0.0" max="1.0">
                    <ColorMap>
                        <Color value="1.0" color="red"/>
                        <Color value="0.5" color="yellow"/>
                        <Color value="0.0" color="white"/>
                    </ColorMap>
                </Field>
            </Plot>
            <Plot>
                <Cells value="d_s" min="0.0" max="0.2">
                    <ColorMap>
                        <Color value="1.0" color="red"/>
                        <Color value="0.5" color="yellow"/>
                        <Color value="0.0" color="white"/>
                    </ColorMap>
                </Cells>
            </Plot>
        </Gnuplotter>
        <DependencyGraph format="svg" exclude-plugins="Logger, Gnuplotter"/>
    </Analysis>
    <CellPopulations>
        <Population size="0" type="cells">
            <InitRectangle mode="random" number-of-cells="50">
                <Dimensions size="size.x, size.y, 0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
        <Population size="0" type="tumor">
            <InitRectangle mode="random" number-of-cells="50">
                <Dimensions size="size.x, size.y, 0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction/>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>5</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>

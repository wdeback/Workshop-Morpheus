Morpheus: an illustrated tutorial
-----------------

## Table of contents

- [Introduction](#introduction)
- [Quick start](#quick-start)
- [My first model](#my-first-model)
- [Cell-based modeling](#cell-based-modeling)
- [Multi-scale modeling](#multi-scale-modeling)

## Introduction

In this tutorial, we'll have a first look at Morpheus and how to construct your first models. Specifically, we will

1. Check out the GUI and run an example model,
2. See the main components of a model,
3. Construct a simple ODE model, export and visualize the data,
4. Build a simple multicellular model and visualize cells,
5. Combine these models to create a multi-scale model.

I will assume you have basic knowledge of ordinary differential equations (ODEs) and the cellular Potts model (CPMs). I'll also assume you have Morpheus (v1.9.2+) installed, together with Gnuplot which is used for plotting.

## Quick start 

If you start Morpheus, you will be faced with an interface like this:

> Things may look a little different on your computer. You can  drag and drop panels around to your liking. 

<img src="./images/1.png" width="80%" />


- In the top left, we have the **Documents** panel where you can browse through sections of a model. And, if you have multiple models opened at once, you can browse between different models. 
  - The black sections are already filled in, the model sections that are greyed out can be added or activated by a double-click.

- In the bottom left, you see the **JobQueue** panel where you can browse through your simulation results.
  - When starting a new simulation, a job will be added to the queue and executed when resources are available.

- In the middle, is the **Editor** panel where most of the modeling will happen as we will see later.

- To the right, we see the **Documentation** panel here you can browse the docs.
  - The documentation panel is context-sensitive, i.e. whenever you select a part of a model, the relevant documentation will appear. 


### Open an example model

Morpheus has a number of example models to help get you started. From the menu bar, just select Examples and select one, e.g. `ODE / PredatorPrey`.

<!---
<img src="./images/2.png" style="width: 40%; display: block; margin: auto; box-shadow: 1px 3px 7px grey" />
--->

<img src="./images/2.png" width="40%" />



#### Front page

Afterwards, you'll see a kind-of front page with a

- **Title**: Descriptive name, also used as a folder name to store simulation results.

- **Description**: Model description containing e.g. functionalities, change log, notes, references. Only for human readability.

- **Symbol graph**: A graphical sketch of the model structure showing how symbols depend on ecah other. 

<img src="./images/4.png" width="80%" />


#### Symbol graph
 
In the example above, it shows that there is a `System` (or ordinary differential equations) that takes a number of symbols (`r`, `m`, `c`) as inputs and writes to symbols `N` and `P`. These are subsequently used by a `Logger`. We can also see the `System` is updated every 0.1 time steps and the `Logger` only every 5 time steps.

### Browsing the model 

You can browse the model in the **Documents** panel. Selecting e.g. the `Global` section, it will show up in the **Editor** and show the `Constants` and `DiffEqns` (differential equations) that make up the `System`. 

You can chang values and expressions by selecting a constant of equation in the editor and alter them in the **Attribute** editor in the top right.

<img src="./images/5.png" width="80%" />

#### Running the model

To run the model, simply push the **Start** button or press **F8**. The model is now added to the **JobQueue**. 

<img src="./images/6.png" width="80%" />

#### Viewing results

To see simulation results, select the job. This open a **file browser** opens in the middle panel.

In the file browser, you can preview files:

- To view plots, select an **image file** (`*.png`, `*.jpg`) to open it in the right panel.

- To view log files, select a **text file** (`*.txt`, `*.xml`) to preview it in the right panel (max. `10Mb`).

> For 3D simulations, Morpheus writes `TIFF` and `VTK` files, but they need to be rendered in external software such as [Fiji/ImageJ](http://fiji.sc) or [Paraview](https://www.paraview.org/). 

The center bottom panel shows the **standard output** of the model. At the end of simulation, it shows information on **execution time** (wall time and cpu time) and **memory usage**.

#### Check out other models

Morpheus has a number of built-in models that show different features. Have a look around!

```
├── CPM
│   ├── CellSorting_2D.xml
│   ├── CellSorting_2D2.xml
│   ├── CellSorting_3D.xml
│   ├── ConvergenceExtension.xml
│   ├── ConvergentExtension.xml
│   ├── Crypt.xml
│   ├── Persistence_2D.xml
│   ├── PigmentCells.xml
│   ├── Proliferation_2D.xml
│   ├── Proliferation_3D.xml
│   ├── Protrusion.xml
│   ├── Protrusion_2D.xml
│   ├── Protrusion_3D.xml
│   ├── RunAndTumble.xml
│   └── crypt.tif
├── Miscellaneous
│   ├── FrenchFlag.xml
│   ├── GameOfLife.xml
│   ├── GameOfLife_Field.xml
│   ├── ParticleAggregation.xml
│   └── ShellCA.xml
├── Multiscale
│   ├── AutocrineChemotaxis.xml
│   ├── CellCycle.xml
│   ├── CellCycle_3D.xml
│   ├── CellCycle_PDE.xml
│   ├── CellPolarity.xml
│   ├── CellPolarity_3D.xml
│   ├── Dictyostelium.xml
│   ├── MultiscaleModel.xml
│   ├── PCP.xml
│   ├── PlanarCellPolarity.xml
│   └── VascularPatterning.xml
├── ODE
│   ├── CellCycle.xml
│   ├── CellCycleDelay.xml
│   ├── CellCycle_Global.xml
│   ├── DeltaNotch.xml
│   ├── LateralSignaling.xml
│   ├── MAPK_SBML.xml
│   └── PredatorPrey.xml
└── PDE
    ├── ActivatorInhibitor_1D.xml
    ├── ActivatorInhibitor_2D.xml
    ├── ActivatorInhibitor_Domain.xml
    ├── ExcitableMedium_3D.xml
    ├── TuringPatterns.xml
    └── domain.tif
```


## My first model

Running examples is nice, but constructing a model from scratch is something completely different! 

Here, we will go through the common steps and traps when building a new model.


### Basic model structure

To start a new model, `File -> New` in the menu. A model opens in the Document panel.

Each model needs to specify some parts. This includes the `Description` section that we saw above and the sections `Space` and `Time`. For most simulations, we will also need a `Global` section to define some variables and a `Analysis` section to generate output. Let's have a look at these sections. 

> Required sections are highlighted in black, optional sections are greyed out and can be added by double-click.

#### `Space`

In the `Space` section, you define the size and structure of the lattice (discretized space) as well as e.g. the boundary conditions.

For instance, to construct a simulation in a 2D square space of size 100 x 100: 

<!---
<img src="./images/7.png" style="width: 80%;  display: block; margin: auto; box-shadow: 1px 3px 7px grey" />
 --->

<img src="./images/7.png" width="80%" />

Optionally, you can `BoundaryConditions` or read a `Domain` from file by selecting (double-click) one of the optional plugins that appear in the bottom-right panel:

> By default, periodic boundaries are used.  

<img src="./images/8.png" width="30%" />


#### `Time`

In the `Time` section, you specify the duration of a simulation in terms of simulation steps. You can also specify a symbol to refer to the current time (by default, this is `time`).

Here, a simulation is specified to run from for a `1000` steps:
  
<img src="./images/9.png" width="80%" />

Optionally, you can choose a `RandomSeed` to make a simulation exactly reproducible. By default, a different seed is used in each simulation run (based on the current time). 

> If using multithreading (`Settings / Local / *Threads per job*`), make sure to use the same number of threads to exactly reproduce simulation results, because each thread has its own random seed.

#### `Global`

In the `Global` scope, you can start doing some real modeling! 

> The name `"Global"` comes from the fact that Morpheus uses [scoping](https://en.wikipedia.org/wiki/Scope_(computer_science)). As in almost all programming languages, variables are only valid for the scope which they are defined. In Morpheus, each symbol is only accessible within its own scope. For example, a symbol attached to one cell type is only valid for points in space that are occupied by those cells and not anywhere else. Scoping is a pretty powerful concept, but more on that later. 

In `Global` you can declare `Constants`, `Variables` and `Fields` that are valid throughout the simulation. 

Here we specify 3 `Variables` and change their values using a `System` of `DiffEqn` (taken from [`CellCycle_Global.xml`](./models/CellCycle_Global.xml)): 

<img src="./images/10.png" width="80%" />

Note that we can choose a `solver` and `time-step` for the `System`. Here, we also specify an option `time-scaling` to scale the time of the ODE system to the simulation time, making it 50x slower.

#### `Analysis`

Finally, we want the simulation to give us some output.

The section `Analysis` is used for data export and plotting. It hosts a large number of tools to track and visualize cells and their properties, their motility, cell-cell contacts, etc. 

The `Logger` and `Gnuplotter` are probably the most relevant for common purposes. 

Here, I specify a `Logger` to periodically write the values of our 3 symbols (APC, CDK1 and Plk1) to a CSV file.  Additionally, we define a `Plot` in which the values of these symbols are plotted against `time`.

<img src="./images/11.png" width="80%" />

This will produce a tab-delimited text file called `logger.txt`:

```bash
"time"	"APC"	"CDK1"	"Plk1"
0	0	0	0
1	2.94419e-174	0.002	4.608e-22
2	2.10491e-150	0.004	2.23705e-19
3	2.3242e-137	0.006	8.55537e-18
4	3.03672e-128	0.008	1.13653e-16
5	3.33834e-121	0.01	8.4497e-16
6	1.87021e-115	0.012	4.35098e-15
7	1.35985e-110	0.014	1.73875e-14
8	2.22359e-106	0.016	5.77169e-14
9	1.16268e-102	0.018	1.66271e-13
10	2.47079e-99	0.02	4.28328e-
```

**and** an image file called `logger_plot_time_APC_01000.png`:

<img src="./images/12.png" width="40%" />


---

> Download the model: [tutorial_1.xml](./models/tutorial_1.xml) 

---

## Cell-based modeling

You have now seen the GUI and created and simulated a simple ODE model from scratch. But you're probably actually interested in simulating multicellular systems. 

Enter: cells!

We'll extend the model above to simulate a population of growing cells. In the process, we'll need the remaining sections: `CellTypes`, `CPM` and `CellPopulations`. 

### `CellTypes`

This is where most of the action happens. Here, we can define one of multiple `CellType`s, specify their properties, behavior and interactions with the microenvironment. We will move a little slower here to make sure things get clear. 

Here, we model a population of with cells with the [cellular Potts model](https://en.wikipedia.org/wiki/Cellular_Potts_model).

- First, you give the `CellType` a name, like `"cells"`:

<img src="./images/13.png" width="80%" />

There is a long list of plugins that you can use within a `CellType` in different categories: containers, mathematical expressions, motility, population, reporters and cell shape. They can be added to the model via the bottom-right panel or the context menu: `right-click -> Add`. 

<img src="./images/16.png" width="30%" />

- From the list, select the `VolumeConstraint` and to set the target volume (or area in 2D) to 100 lattice sites (i.e. pixels).

- Also add a `SurfaceConstraint` (with default params) to constrain the cell surface (or perimeter in 2D) to keep approximately circular.

<img src="./images/15.png" width="80%" />

We'll return to specify our `CellType` more later -- we'll add cell division and a cell cycle model. First, however, let's see how we can get to run what we have so far. 

### `CPM`

In the `CPM` section, you specify the parameter for the cellular Potts model (CPM). In particular:

- `MonteCarloSampler` that produces the dynamics in the CPM. You'll need to specify:
  - `MCSDuration`: how long a Monte Carlo step (one full lattice update) takes in terms of simulation time. Set to `1` as a default.
  - `Neighborhood`: size of neighborhood from which to sample spin-copy attempts. Set to `Order=2` (Moore neighborhood) as a default.
  - `MetropolisKinetics`: the `temperature` set the amount of fluctuations/randomness in the system. The higher the `temperature`, the more energetically unfavorable update are accepted. If `temperature=0`, no energetically unfavorable updates are allowed which settles the system in local minima. Set to `1` as a reasonable default.

<img src="./images/17.png" width="80%" />

- `Interaction` energies that control the 'adhesive' forces between cells, but we'll leave them out for now (they are `0` by default).

### `CellPopulations`

If you now execute the model, you will see the following error message:

<img src="./images/18.png" width="40%" />

Morpheus detects that you specified one of more `CellType`s but did not specify any initial configuration for them. 

Thus, you'l have to define the initial spatial configuration of cells. You can arrange cells as psingle pixels in a rectangular or circular grid with `InitRectangle` or `InitCircle`, as box or spherical objects with `InitObjects` and read complex initial conditions from image file using the `TIFFReader`.

Here, however, we just use the default initializer and let a population of 100 cells be randomly placed. For this, we only need to specify the `size` to be `100` and `type` to be `cells`:

<img src="./images/19.png" width="80%" />

At this point, we can execute the model with any error. However, we won't actually have any visual output of the cells. 

### `Gnuplotter`

To visualize cells in 2D, you can use the `Gnuplotter` plugin in the `Analysis` section. 

Here, we specify it plot `Cells` every 100 `time-step`, color-coded by their `cell.id`:

<img src="./images/20.png" width="80%" />

Now, when you execute the model, Morpheus will generate plots in filenames like `plot_01000.png`:

<img src="./images/21.png" width="40%" />

You have just created your first cell-based model with Morpheus!

---

> Download this model: [tutorial_2.xml](./models/tutorial_2.xml) 

---

# Multi-scale modeling

Why stop there? Let's try and couple the cell-based CPM model with the cell cycle model to create a multi-scale model. Here, we want a intracellular ODE model representing the gene network of cell cycle genes to trigger the division of our CPM cells.

### Intracellular `System`

To accomplish this, we will re-use the cell cycle model we defined in the `Global` section. Here, however, the cell cycle should be computed *for every individual cell*. This is done by defining the same model in the scope of our `CellType`. 

First, you define three properties using the `Property` container (named APC, CDK1 and Plk1). These are just variables, but are attached to each individual cell in the population of our cell type.

To define the `System`, we can **copy and paste** (via context menu) it from the `Global` to our `CellType`. You can now remove, cut or disable the obsolete `System` from `Global`.

> If you have multiple models opened, you can also copy and paste between different models.

Your `CellType` definition should look something like this: 

<img src="./images/22.png" width="40%" />

Note that I used a random number between 0 and 1 (`rand_uni(0,1)`) as an initial value for the `CDK1` property such that each cell starts in a different phase in the cell cycle model. 

If you color-code the cells according to their `CDK1` property in `Gnuplotter`, the simulation output will now look like this:

<img src="./images/23.png" width="40%" />

> To change the color-coding, go to `Analysis / Gnuplotter` and choose a relevant symbol in `Plot / Cells / value`.

### Coupling scales

Each individual cell now has its own cell cycle model. However, it is not coupled to the CPM in any way yet. Let's couple the ODE models to regulate their cell divisions. 

In our `CellType`, you add `CellDivision` and specify the `Condition` for this event to depend on the cell's value of `CDK1`. Here, we let cells divide when `CDK1 > 0.5`. 

<img src="./images/24.png" width="40%" />

To prevent cells from dividing indefinitely when they reach this value, you can define a `Rule` to reset `CDK1=0` after division in the `Triggers` section.

Now, starting from a population of 5 cells (in `CellPopulations`, set `Population / size = 5`), you obtain a result like this:

<img src="./images/25.png" width="40%" />

Here, each color depicts a clone of cells that share a common ancestor and are (therefore) synced in their cell cycle.

---

> Download this model: [tutorial_3.xml](./models/tutorial_3.xml) 

---

**Congrats!** You have just created a multi-scale model in Morpheus. 

Here is the symbol dependency graph for this model:

<img src="./images/26.png" width="60%" />

> The different shades of blue indicate the scopes (`Global`, `CellTypes` and `System`).  

---

# Next up

**Please continue to the [tutorial on parameter exploration](https://gitlab.com/wdeback/Workshop-Morpheus-Nottingham/blob/master/tutorial-paramsweep/README.md)  in Morpheus.**

---

